#include <iostream>
#include<algorithm>

using namespace std;

struct Event {
    int start, end;
};
bool cmpStart(Event a, Event b) {
	return a.start > b.start;
}
bool cmpEnd(Event a, Event b) {
	return a.end > b.end;
}
int how_many_rooms (Event* events, int N) {
	sort(events, events + N, cmpStart);
	int rums = 0;
	int macrums = rums;
	for (int i = 0; i < N; i++) {
		++rums;
		for (int j = 0; j < i; ++j) {
			if (events[i].start > events[j].end) rums--;
		}
		if (macrums < rums) macrums = rums;
	}
	return macrums;
}

int main() {
    int N;
    cin >> N;
    Event e[N];
    for (int i=0; i<N; i++) cin >> e[i].start >> e[i].end;

    cout << how_many_rooms(e, N) << endl;
}
