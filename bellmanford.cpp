#include <iostream>
#include <climits>
#include <vector>
#include <utility>

using namespace std;

const int INF = INT_MAX;

typedef struct Node
{
    vector <pair<Node *, int>> neighbours;
    int val, dist;
} Node;

Node *newNode(int val)
{
    Node *n = new Node;
    n->val = val;
    n->dist = INF;
    return n;
}

void addEdge(Node *u, Node *v, int w)
{
    pair <Node *, int> edge(v, w);
    u->neighbours.push_back(edge);
}

bool relax(Node *u, Node *v, int w)
{
    if(v->dist > u->dist + w)
    {
        v->dist = u->dist + w;
        return true;
    }
    return false;
}

bool bellmann_ford(Node **nodes, int n, Node *start)
{
    start->dist = 0;
    int i = n-1;
    while(i--)
    {
        for(int i = 0; i < n; i++)
        {
            for(pair<Node*, int> v : nodes[i]->neighbours)
            {
                relax(nodes[i], v.first, v.second);
            }
        }
    }
    for(int i = 0; i < n; i++)
    {
        for(pair<Node*, int> v : nodes[i]->neighbours)
        {
            if(relax(nodes[i], v.first, v.second))
                return false;
        }
    }
    return true;
}

int main()
{
    int N, k, start, end;
    cin >> N >> k;
    Node *nodes[N];
    for (int i = 0; i < N; i++)
        nodes[i] = newNode(i);

    for (int i = 0; i < k; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;
        addEdge(nodes[u], nodes[v], w);
    }

    cin >> start >> end;
    if (!bellmann_ford(nodes, N, nodes[start]))
        cout << "CYKL" << endl;
    else if (nodes[end]->dist < INF)
        cout << nodes[end]->dist << endl;
    else
        cout << "NIE" << endl;

    for(int i=0; i<N; i++)
        delete nodes[i];
}

