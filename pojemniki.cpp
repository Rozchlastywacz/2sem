#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<iomanip>
using namespace std;

#define D 20

struct node
{
    int value;
    int width;
    int index;
    int vol;
    node* next;
    node* complementary;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
node* createListR(int numberOfElements, int repeat, int wid)
{
    if(repeat==2){numberOfElements--; repeat=0; wid=ranGen();}
    node* elem = new node;
    elem->value = ranGen();
    elem->width = wid;
    elem->index = numberOfElements;
    elem->next = numberOfElements > 1 ? createListR(numberOfElements, repeat+1,wid) : NULL;
    return elem;
}
node* createList(int numberOfElements)
{
    node* head = NULL;
    node** last = &head;
    while(numberOfElements > 1)
    {
        int wid = ranGen();
        node* elem1 = new node;
        node* elem2 = new node;
        *last = elem1;

        elem1->value = ranGen();
        elem1->width = wid;
        elem1->index = numberOfElements;
        elem1->next = elem2;
        elem1->complementary = elem2;

        elem2->value = ranGen();
        elem2->width = wid;
        elem2->index = numberOfElements;
        elem2->next= NULL;
        elem2->complementary = elem1;

        elem1->vol=elem2->vol=wid*(elem1->value>elem2->value ? elem1->value-elem2->value : elem2->value-elem1->value);

        last = &(elem2->next);
        numberOfElements--;
    }
    return head;
}
void dumpList(node* iter)
{
    while(iter)
    {
        cout << setw(2) << iter->value << "|"
        << setw(2) << iter->width << "|"
        << setw(4) << iter->vol << "|"
        << setw(2) << iter->index << "|-|"
        << endl;
        iter = iter->next;
    }
    cout << endl;
}
void splitting(node* primalHead, node* &firstHead, node* &secondHead)
{
    if(primalHead==NULL || primalHead->next==NULL)
        return;
    secondHead = firstHead = primalHead;
    node* previous;
    while(primalHead!=NULL)
    {
        previous = secondHead;
        secondHead = secondHead->next;
        primalHead = primalHead->next;
        if(primalHead!=NULL)
            primalHead = primalHead->next;
    }
    previous->next = NULL;
}
void mergeList(node* &primalHead, node* firstHead, node* secondHead)
{
    node** iteratorre = &primalHead;
    while(firstHead!=NULL && secondHead!=NULL)
    {
        if(firstHead->value < secondHead->value)
        {
            *iteratorre = firstHead;
            firstHead = firstHead->next;
        }
        else
        {
            *iteratorre = secondHead;
            secondHead = secondHead->next;
        }
        iteratorre = &((*iteratorre)->next);
    }
    if(firstHead==NULL) *iteratorre = secondHead;
        else if(secondHead==NULL) *iteratorre = firstHead;
}
void mergeSort(node* &head)
{
    if(head==NULL || head->next==NULL)
        return;
    node* firstHead;
    node* secondHead;
    splitting(head, firstHead, secondHead);
    mergeSort(firstHead);
    mergeSort(secondHead);
    mergeList(head, firstHead, secondHead);
}
int howManyLevels(node* head, int water)
{
    node* iter = head;
    int wid = 0;
    int volume = 0;
    int currentLevel = iter->value;
    int lastLevel = iter->value;
    while (iter!=NULL && volume<water)
    {
        //if(iter->value==currentLevel) wid+=iter->width;
        node* iter1 = head;
        while((iter1->value)<=currentLevel && currentLevel<=((iter1->complementary)->value))
        {
            volume+=((iter->width)*(-(iter->value)+currentLevel));
            iter1=iter1->next;
        }
            //volume+=((iter->width)*(iter->value-currentLevel));
            lastLevel=currentLevel;
            currentLevel = iter->value;
            cout << currentLevel << "-" << volume << endl;
            iter = iter->next;
            volume=0;
    }
    int counter = 0;
    while((head->value)<=lastLevel && ((head->complementary)->value)<=lastLevel)
    {
        counter++;
        head = head->next;
    }
    return counter;
}
int main()
{
    srand (7);
    int V = 168;
    /*node* lista = createListR(5,0, ranGen());
    dumpList(lista);
    mergeSort(lista);
    dumpList(lista);
    cout << "zostanie zalane: " << howManyLevels(lista, V) << " poziomow" << endl;
    */node* newlist = createList(5);
    cout << "nowaLista" << endl;
    dumpList(newlist);
    mergeSort(newlist);
    dumpList(newlist);
    cout << "zostanie zalane: " << howManyLevels(newlist, V) << " pojemnikow" << endl;
    return 0;
}
