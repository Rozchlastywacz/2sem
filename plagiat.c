#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int MAX_TEXT_LENGTH = 200;
int min(int a, int b, int c){
    if(a < b && a < c) return a;
    else if(b < a && b < c) return b;
    else return c;
}
int distance(char* a, int a_length, char* b, int b_length) {
    int cost;
    if(a_length == 0) return b_length;
    if(b_length == 0) return a_length;

    if(a[a_length-1] == b[b_length-1]) cost = 0;
    else cost = 1;

    return min( distance(a, a_length - 1, b, b_length    ) + 1,
                distance(a, a_length    , b, b_length - 1) + 1,
                distance(a, a_length - 1, b, b_length - 1) + cost );
}
int distanceNR(char* a, int a_length, char* b, int b_length) {
    unsigned int lastdiag, olddiag;
    unsigned int *column = malloc((a_length+1)*sizeof(int));
    for (int y = 1; y <= a_length; y++)
        column[y] = y;
    for (int x = 1; x <= b_length; x++) {
        column[0] = x;
        for (int y = 1, lastdiag = x-1; y <= a_length; y++) {
            olddiag = column[y];
            column[y] = min(column[y] + 1, column[y-1] + 1, lastdiag + (a[y-1] == b[x-1] ? 0 : 1));
            lastdiag = olddiag;
        }
    }
    return(column[a_length]);
}

int main() {
    char* a = malloc(MAX_TEXT_LENGTH*sizeof(char));
    char* b = malloc(MAX_TEXT_LENGTH*sizeof(char));
    scanf("%[^\n]", a);
    scanf("%c\n", b); // reads \n
    scanf("%[^\n]", b);
    printf("%d\n", distanceNR(a, strlen(a), b, strlen(b)));
}
