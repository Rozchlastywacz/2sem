#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<iomanip>
using namespace std;

#define D 20

struct node
{
    int value;
    node* next;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
node* createListR(int numberOfElements)
{
    node* val = new node;
    val->value = ranGen();
    val-> next = numberOfElements > 1 ? createListR(numberOfElements - 1) : NULL;
    return val;
}
void dumpList(node* iter)
{
    while(iter)
    {
        cout << setw(2) << iter->value << "|";
        iter = iter->next;
    }
    cout << endl;
}
int main()
{
    srand (7);
    node* lista = createListR(20);
    dumpList(lista);
    return 0;
}

