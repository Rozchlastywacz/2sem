#include<stdlib.h>
#include<iomanip>
#include<iostream>
#define D 20
#define N 1000
using namespace std;

int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
void fillTable(bool table[N][N], int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            if(ranGen()<4*D/5) table[i][j] = true;
            else table[i][j] = false;
        }
    }
}
void coutTable(bool table[N][N], int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            table[i][j] == true ? cout << " 1 " : cout << " 0 ";
        }
        cout << endl;
    }
    cout << endl;
}
void lookForPaths(bool table[N][N], int n, int x, int y, int &counter){
    if(((x<0 || x >=n) || (y<0 || y>=n)) || !table[x][y]) return;
    counter++;
    table[x][y] = false;
    lookForPaths(table, n, x+1, y, counter);
    lookForPaths(table, n, x-1, y, counter);
    lookForPaths(table, n, x, y+1, counter);
    lookForPaths(table, n, x, y-1, counter);
}
int main(){
    bool table[N][N];
    fillTable(table, N);
    //coutTable(table, N);
    int x;
    int y;
    cin >> x;
    cin >> y;
    int counter = 0;
    lookForPaths(table, N, x, y, counter);
    cout << counter;
}
