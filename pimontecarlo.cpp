#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;

double MCpi(double iterations)
{
    double counter = 0;
    double x;
    double y;
    for(int i = 0; i < iterations; i++)
    {
        x = (rand() % 1000/1000.0d);
        y = (rand() % 1000/1000.0d);
        if(x*x+y*y <= 1) counter++;
    }
    return 4.0*(counter/iterations);
}
int main()
{
    srand(time(NULL));
    cout << "pi wynosi: " << MCpi(1000000) << endl;
    return 0;
}
