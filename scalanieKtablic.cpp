#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<iomanip>
using namespace std;

#define D 20

struct node
{
    int k;
    int tab[3];
    node* next;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
void boardGen(int board[], int k)
{
    for(int i = 0; i < k; i++)
    {
        board[i] = ranGen();
    }
}
node* createListR(int numberOfElements)
{
    node* elem = new node;
    elem->k = 3;
    boardGen(elem->tab, elem->k);
    elem->next = numberOfElements > 1 ? createListR(numberOfElements - 1) : NULL;
    return elem;
}
void dumpBoard(int board[], int n)
{
    for(int i = 0; i < n; i++)
    {
        cout << board[i] << "|";
    }
    cout << "--|";
}
void dumpList(node* iter)
{
    while(iter)
    {
        dumpBoard(iter->tab, iter->k);
        iter = iter->next;
    }
    cout << endl;
}
int main()
{
    srand (7);
    node* lista = createListR(3);
    dumpList(lista);
    return 0;
}


