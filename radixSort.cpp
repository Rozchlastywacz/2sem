#include<iostream>
#include<iomanip>
#include<time.h>
#include<stdlib.h>

#define N 21
#define D (21*21-1)

using namespace std;
int width(int number)
{
    int counter = 0;
    while(number%10!=0)
    {
        counter++;
        number=number/10;
    }
    return counter;
}
int ranGen(void)
{
    int number = (rand() % D);
    return number;
}
void makeBoard(int board[], int n)
{
    for(int i = 0; i < n; i++)
    {
        board[i] = ranGen();
    }
}
void dumpBoard(int board[], int n)
{
    for(int i = 0; i < n; i++)
    {
        cout << setw(4) << board[i] << "|";
    }
    cout << endl;
}
void radixSort(int board[], int n)
{
    int help[n];
    int mod = 10;
    int counter = 0;
    while(mod/10 <= n*n)
    {
        int iter = 0;
        for (int i = 0; i < 10; i++)
        {
            for(int j = 0; j < n; j++)
            {
                counter++;
                if((board[j]%mod)/(mod/10) == i) help[iter++] = board[j];
            }
        }
        for(int i = 0; i < n; i++) board[i] = help[i];
        mod*=10;
        cout << counter <<" pomocnicza:" << endl;
        dumpBoard(help, n);
    }
}
int main()
{
    srand(7);
    int board[N];
    makeBoard(board, N);
    dumpBoard(board, N);
    radixSort(board, N);
    cout << "wynikowa" <<endl;
    dumpBoard(board, N);
}
