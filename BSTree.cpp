#include<iostream>
#include<stdlib.h>
#include<time.h>
#include<iomanip>
using namespace std;
#define D 20
struct node
{
    int value;
    node* parent;
    node* leftSon;
    node* rightSon;
    int leftSubTree;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
void put(node* &root, int element)
{
    cout << "putting " << element << endl;
    node** iter = &root;
    node* ociec = NULL;
    while((*iter)!=NULL)
    {
        ociec = *iter;
        if(element >= (*iter)->value)
            iter = &((*iter)->rightSon);
        else
        {
            (*iter)->leftSubTree++;
            iter = &((*iter)->leftSon);
        }
    }
    node* tmp = new node;
    tmp->leftSon = NULL;
    tmp->rightSon = NULL;
    tmp->value = element;
    tmp->parent = ociec;
    tmp->leftSubTree = 0;
    *iter = tmp;
}
void printTree(node* root)
{
    if(root==NULL)
        return;
    printTree(root->leftSon);
    cout << root->value << " - leftSubtree = " << root->leftSubTree << endl;
    printTree(root->rightSon);
}
node* growATree(int howManyElements)
{
    node* root = NULL;
    while(howManyElements--)
        put(root, ranGen());
    return root   ;
}
int searchForIthElement(node* root, int i)
{
    if(root->leftSubTree+1 == i)
        return root->value;
    else if(i > root->leftSubTree)
        return searchForIthElement(root->rightSon, i - 1 - root->leftSubTree);
    else
        return searchForIthElement(root->leftSon, i);
}
int whichElementIsX(node* root, int x, int i)
{
    if(root->value == x)
        return (i + root->leftSubTree + 1);
    else if(x > root->value)
        return whichElementIsX(root->rightSon, x, i+root->leftSubTree+1);
    else
        return whichElementIsX(root->leftSon, x, i);
}
int whichElementIsX(node* root, int x)
{
    if(root == NULL)
        return 0;
    else if(x < root->value)
        return whichElementIsX(root->leftSon, x);
    else
        return root->leftSubTree + 1 + whichElementIsX(root->rightSon, x);
}
int index(node* root)
{
    int l = root->leftSubTree;
    while(root->parent!=NULL)
    {
        if(root->parent->leftSon==root)
            root = root->parent;
        else
        {
            root = root->parent;
            l+= root->leftSubTree+1;
        }
    }
    return l;
}
int tu_se_jebneA(node * p, int a)
{
    while(p!=NULL)
    {
        if(p->leftSubTree==a-1)
            return p->value;
        if(p->leftSubTree>a-1)
            p=p->leftSon;
        else
        {
            a-=p->leftSubTree+1;
            p=p->rightSon;
        }
    }

    return NULL;
}

int a_tu_jebneB(node* p, int val)
{
    int counter=1;

    while(p->value != val)
    {
        if(val<p->value)
        {
            counter;
            p=p->leftSon;
        }
        else
        {
            counter+=1+p->leftSubTree;
            p=p->rightSon;
        }
    }

    return counter+p->leftSubTree;
}

int main()
{
    srand(7);
    node* three = growATree(10);
    printTree(three);
    while(true)
    {
        int i;
        cin >> i;
        if(i >= 0)
            cout << index(three)<< endl;
        else
            break;
    }
}
