#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define D 20

struct node
{
    int value;
    struct node* next;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
struct node* createListR(int numberOfElements)
{
    node* elem = new node;
    elem->value = ranGen();
    elem->next = numberOfElements > 1 ? createListR(numberOfElements - 1) : NULL;
    return elem;
}
void dumpList(node* iter)
{
    while(iter)
    {
        printf("d%s%", iter->value, "|");
        iter = iter->next;
    }
    printf("/n");
}
int main()
{
    srand (7);
    node* lista = createListR(20);
    dumpList(lista);
    return 0;
}
