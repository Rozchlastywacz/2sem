#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
    int value;
    struct node* next;
} node;

node* reverse(node* list) {
    //if(list!=NULL) printf("|%d|\n", list->value);
    if(list==NULL || list->next==NULL) return list;
    node* head = list;
    node* tail = list->next;
    head->next = NULL;
    node* tmp = reverse(tail);
    tail->next = head;
    return tmp;
}
int main() {
    int n;
    int val;
    scanf("%d", &n);
    node* list = malloc(sizeof(node));
    list->next = NULL;
    node* tmp = list;
    for (int i=0; i<n; i++) {
        scanf("%d", &val);
        tmp->next = malloc(sizeof(node));
        tmp = tmp->next;
        tmp->value = val;
        tmp->next = NULL;
    }
    tmp = list->next;
    free(list);

    list = reverse(tmp);

    while(list != NULL) {
        printf("%d\n", list->value);
        tmp = list;
        list = list->next;
        free(tmp);
    }
}
