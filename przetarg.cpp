#include <stdio.h>
// we're big guys now, let's use already implemeted list
// you might want check this out: http://www.cplusplus.com/reference/list/list/#functions
#include <list>

using namespace std;

// Global variables are bad, but in this case
// passing them in function would soon exceed memory limit.
// Trust me, I tried.
int n;
list<int> *graph;
list<int> *line;


void euler(int u) {
    while (!graph[u].empty())
    {
        int v = *(graph[u].begin());
        list<int>::iterator it = graph[v].begin();
        for (; it != graph[v].end(); it++)
        {
            if ((*it) == u)
            {
                graph[v].erase(it);
                graph[u].erase(graph[u].begin());
                break;
            }
        }
        euler(v);
    }
    (*line).push_front(u);
}
void printLine(list<int> lines){
        printf("1\n");
        printf("%lu ", lines.size());
        for (list<int>::iterator it = lines.begin(); it!=lines.end(); it++) {
            printf("%d ", *it);
        }
}
int main() {
    int Z;

    scanf("%d", &Z);

    while (Z--) {

        int m, u, v;
        scanf("%d %d", &n, &m);

        graph = new list<int>[n+2];
        line = new list<int>;

        for(int i=0; i<m; i++) {
            scanf("%d %d", &u, &v);
            graph[u].push_back(v);
            graph[v].push_back(u);
        }

        int start = 1;
        for (int i = 1; i <= n; i++)
        {
            // find starting city for euler
            // it must be a vertex with odd number of neighbours if it exists
            // or any one if every one has even degree
            if (graph[i].size()%2!=0)
            {
                start = 0;
                graph[i].push_back(start);
                graph[start].push_back(i);
            }
        }

        // start euler from correct city
        euler(start);
        //obluga usuwanie sztucznego wierzcholka
        list< list<int> > *lines;
        lines = new list< list<int> >;
        (*lines).push_back(new list<int>);
        list< list<int> >::iterator linesIter = lines->begin();

        for (list<int>::iterator it = line->begin(); it != line->end(); it++) {
            if((*it)!=0){
                (*linesIter).push_back(*it);
            }
            else{
                printLine(*linesIter);
                linesIter++;
                linesIter = new list<int>;
            }

        }

        printf("\n");

        delete [] graph;
        delete line;
    }
}

