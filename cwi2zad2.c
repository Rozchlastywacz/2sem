#include <stdio.h>
#include <stdlib.h>

int divider(int* tab, int n, int p, int k, int sum){
  if(p==k) return p;
  return sum<0 ? divider(tab, n, p+1, k, sum+tab[p])
    : divider(tab, n, p, k-1, sum-tab[k]);
}
int find_divider(int* tab, int n) {
  int sum = 0;
  int p = 0;
  int k = n-1;
  return divider(tab, n, p, k, sum);
}

int main() {
    int n;
    scanf("%d", &n);
    int* tab = malloc(n * sizeof(int));
    for (int i=0; i<n; i++) {
        scanf("%d", &tab[i]);
    }
    printf("%d\n", find_divider(tab, n));
    free(tab);
}
