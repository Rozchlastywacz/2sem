#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<iomanip>
using namespace std;

#define D 20

struct node
{
    int value;
    node* next;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
node* createListR(int numberOfElements)
{
    node* val = new node;
    val->value = ranGen();
    val-> next = numberOfElements > 1 ? createListR(numberOfElements - 1) : NULL;
    return val;
}
void dumpList(node* iter)
{
    while(iter)
    {
        cout << setw(2) << iter->value << "|";
        iter = iter->next;
    }
    cout << endl;
}
void splitting(node* primalHead, node* &firstHead, node* &secondHead)
{
    if(primalHead==NULL || primalHead->next==NULL)
        return;
    secondHead = firstHead = primalHead;
    node* previous;
    while(primalHead!=NULL)
    {
        previous = secondHead;
        secondHead = secondHead->next;
        primalHead = primalHead->next;
        if(primalHead!=NULL)
            primalHead = primalHead->next;
    }
    previous->next = NULL;
}
void mergeList(node* &primalHead, node* firstHead, node* secondHead)
{
    node** iteratorre = &primalHead;
    while(firstHead!=NULL && secondHead!=NULL)
    {
        if(firstHead->value < secondHead->value)
        {
            *iteratorre = firstHead;
            firstHead = firstHead->next;
        }
        else
        {
            *iteratorre = secondHead;
            secondHead = secondHead->next;
        }
        iteratorre = &((*iteratorre)->next);
    }
    if(firstHead==NULL) *iteratorre = secondHead;
        else if(secondHead==NULL) *iteratorre = firstHead;
}
void mergeSort(node* &head)
{
    if(head==NULL || head->next==NULL)
        return;
    node* firstHead;
    node* secondHead;
    splitting(head, firstHead, secondHead);
    mergeSort(firstHead);
    mergeSort(secondHead);
    mergeList(head, firstHead, secondHead);
}
int main()
{
    srand (7);
    node* lista = createListR(20);
    dumpList(lista);
    mergeSort(lista);
    dumpList(lista);
    return 0;
}

/*
node* morgue(node* f1, node* f2){

    if(f1==nullptr) return f2;
    if(f2==nullptr) return f1;
    if(f1->value<f2->value){

        f1->next=morgue(f1->next, f2);
        return f1;
    }
    else{

        f2->next=morgue(f2->next, f1);
        return f2;
    }

}
*/
