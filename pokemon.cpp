#include<iostream>
#include<iomanip>
#include<stdlib.h>
#include<time.h>
#include<cstdio>
using namespace std;
#define M 10
#define Q 10
#define D 10

struct node{
    int a;
    int b;
};
struct subset{
    int parent;
    int ranking;
};
int ranGen()
{
    int number = (rand() % D);
    return number;
}

node* edgeGen(int m){
    node* edges = (node*)malloc(m*sizeof(node));
    for(int i = 0; i < m; i++){
        edges[i].a = ranGen();
        edges[i].b = ranGen();
    }
    return edges;
}
node* edgeGenByHand(int m){
    node* edges = (node*)malloc(m*sizeof(node));
    for(int i = 0; i < m; i++){
        cin >> edges[i].a;
        cin >> edges[i].b;
    }
    return edges;
}
void printArray(int* array, int n){
    for(int i = 0; i < n; i++)
        cout << i << ". " << array[i] << endl;
    cout << endl;
}
void printEdges(node* edges, int m){
    for(int i = 0; i < m; i++){
        cout << i << ". "<< edges[i].a << "-" << edges[i].b << endl;
    }
    cout << endl;
}
void printVerticies(subset subsets[], int n){
    for(int i = 0; i < n; i++){
        printf("%d - par=%d rank=%d\n", i, subsets[i].parent, subsets[i].ranking);
    }
    cout << endl;
}
void makeSet(subset subsets[], int v){
    subsets[v].parent = v;
    subsets[v].ranking = 0;
}
int findV(subset subsets[], int v){
    if(subsets[v].parent != v){
        subsets[v].parent = findV(subsets, subsets[v].parent);
    }
    return subsets[v].parent;
}
void unionSets(subset subsets[], int v, int u){
    int parV = findV(subsets, v);
    int parU = findV(subsets, u);
    int parVrank = subsets[parV].ranking;
    int parUrank = subsets[parU].ranking;
    if(parVrank == parUrank){
        subsets[parV].parent = parU;
        subsets[parU].ranking++;
    }
    else if (parVrank > parUrank){
        subsets[parU].parent = parV;
    }
    else if (parUrank > parVrank){
        subsets[parV].parent = parU;
    }
}

int* graph_memory(node* edges, int m, node* questions, int q){
    subset* verticies = (subset*)malloc((D)*sizeof(subset));
    int* results = (int*)malloc(q*sizeof(int));
    for(int i = 0; i < q; i++){
        results[i] = -1;
    }
    //printVerticies(verticies, D);
    for(int i = 0; i < (D); i++){
        makeSet(verticies, i);
    }
    //printVerticies(verticies, D);
    for(int i = 0; i < m; i++){
        unionSets(verticies, edges[i].a, edges[i].b);
        //cout << i <<". proba" <<endl;
        //printVerticies(verticies, D+1);
        for(int j = 0; j < q; j++){
            if(results[j] ==-1 && findV(verticies, questions[j].a)==findV(verticies, questions[j].b) && )
            results[j] = i;
        }
    }
    return results;
}

int main(){
    srand(time(NULL));
    int m,q;
    cin >> m;
    node* edges = edgeGen(m);
    printEdges(edges, m);
    cin >> q;
    node* queries = edgeGen(q);
    printEdges(queries, q);
    int* results = graph_memory(edges, m, queries, q);
    printArray(results, q);
}
