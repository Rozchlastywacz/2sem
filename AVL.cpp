#include<iostream>
#include<stdlib.h>
#define D 20

using namespace std;

struct node{
    int val;
    node* left;
    node* right;
    node* parent;
    int balance;
};
node* put(node* &root, int x){
    node* tmp = new node;
    tmp->left = NULL;
    tmp->right = NULL;
    tmp->val = x;
    tmp->balance = 0;

    node** iter = &root;
    node* prev = NULL;
    while((*iter)!=NULL){
        prev = (*iter);
        if(x >= (*iter)->val){
            (*iter)->balance++;
            iter = &((*iter)->right);
        }
        else {
            (*iter)->balance--;
            iter = &((*iter)->left);
        }
    }

    tmp->parent = prev;
    (*iter) = tmp;
    return (*iter);
}


void add(node* &root, int x){
    node* insertedElement = put(root, x);
}

void inOrder(node* root){
    if(root == NULL) return;
    inOrder(root->left);
    cout << root->val << endl;
    inOrder(root->right);
}
void printTree(node* it, string str = "", string str2 = "")
{
    if(it != NULL)
    {
        if(str2 == "")
        {
            printTree(it->right, str, "\xda\xc4");
            cout << str << str2 << it->val << endl;
            printTree(it->left, str, "\xc0\xc4");
        }
        else
        {
            if(str2 == "\xc0\xc4")
            {
                printTree(it->right, str+"\xb3 ", "\xda\xc4");
                cout << str << str2 << it->val << endl;
                printTree(it->left, str+"  ", "\xc0\xc4");
            }
            else
            {
                printTree(it->right, str+"  ", "\xda\xc4");
                cout << str << str2 << it->val << endl;
                printTree(it->left, str+"\xb3 ", "\xc0\xc4");
            }
        }
    }
}
void zapinanieParentaTest(node* root, int x){
    while(root!=NULL){
        if(x >= root->val) root = root->right;
        else root = root->left;

        if(root->val==x){
            cout << "hallo" << root->parent->val << endl;
        }
    }
}
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}

int main(){
    srand(7);
    node* tree = NULL;
    int n = 20;
    while(n--){
        add(tree, ranGen());
    }
    printTree(tree);
}
