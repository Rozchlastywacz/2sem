#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<iomanip>
using namespace std;

#define D 20

struct node
{
    int value;
    node* next;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
node* createListR(int numberOfElements)
{
    node* elem = new node;
    elem->value = ranGen();
    elem->next = numberOfElements > 1 ? createListR(numberOfElements - 1) : NULL;
    return elem;
}
void dumpList(node* iter)
{
    while(iter)
    {
        cout << setw(2) << iter->value << "|";
        iter = iter->next;
    }
    cout << endl;
}
void bubbleSort(node* &first)
{
    if(first==NULL || first->next==NULL)
        return;
    node* terminator = NULL;
    while(terminator != first)
    {
        node** iterator_1 = &first;
        while((*iterator_1)->next != terminator)
        {
            if((*iterator_1)->value > ((*iterator_1)->next)->value)
            {
                node* tmp = *iterator_1;
                *iterator_1 = (*iterator_1)->next;
                tmp->next=(*iterator_1)->next;
                (*iterator_1)->next = tmp;
            }
            iterator_1 = &((*iterator_1)->next);
        }
        terminator = *iterator_1;
    }
}
void insertionSort(node* &first){
    if(first==NULL || first->next==NULL)
        return;

    node* terminator = first->next; //wskaznik na pierwszy nieposortowany element
    while(terminator != NULL){
            cout << "CHUJCHUJCHUJjestem w whilezew" << endl;
        node** iterator_1 = &first;
        //node** iterator_2 = &terminator;
        while(terminator->value < ((*iterator_1))->value){
            cout << "jestem w  whilewew" << endl;
            iterator_1 = &((*iterator_1)->next);
            if((*iterator_1) == terminator) break;
        }
        node* tmp = terminator;
        terminator = terminator->next;
        tmp->next = *iterator_1;
        *iterator_1 = tmp;
    }
}
int main()
{
    srand (7);
    node* lista = createListR(20);
    dumpList(lista);
    insertionSort(lista);
    //bubbleSort(lista);
    dumpList(lista);
    return 0;
}

