#include<iostream>
#include<iomanip>
#include<time.h>
#include<stdlib.h>

#define N 10
#define D 2
//szuka czy jakas liczba nie ma iwecej niz n/2 razy wystapien
using namespace std;
int width(int number)
{
    int counter = 0;
    while(number%10!=0)
    {
        counter++;
        number=number/10;
    }
    return counter;
}
int ranGen(void)
{
    int number = (rand() % D);
    return number;
}
void makeBoard(int board[], int n)
{
    for(int i = 0; i < n; i++)
    {
        board[i] = ranGen();
    }
}
void dumpBoard(int board[], int n)
{
    for(int i = 0; i < n; i++)
    {
        cout << setw(2) << board[i] << "|";
    }
    cout << endl;
}
int zad(int tab[], int size)
{
    int counter = 0;
    int number;
    for(int i=0; i < size; i++)
    {
        if(counter == 0)
        {
            number = tab[i];
            ++counter;
        }
        else
        {
            if(tab[i] != number)
                --counter;
            else
                ++counter;
        }
    }


    if(counter > 0)
    {
        int counter2 = 0;
        for(int i=0; i<size; i++)
            if(tab[i] == number)
                ++counter2;

        if(2*counter2 > size) // counter2 > size/2
            return number;
        else
            return -1;
    }
    else
        return -1;
}
int main()
{
    srand(time(NULL));
    int board[N];
    makeBoard(board, N);
    dumpBoard(board, N);
    cout << zad(board, N) << endl;
}
