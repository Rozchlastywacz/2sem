#include<iostream>
#include<time.h>
#include<stdlib.h>
#include<iomanip>
using namespace std;

#define D 20

struct node
{
    int value;
    int value2;
    node* next;
};
int ranGen()
{
    int number = (rand() % D + 1);
    return number;
}
node* createListR(int numberOfElements)
{
    node* elem = new node;
    elem-> value = ranGen();
    elem-> value2 = ranGen();
    elem-> next = numberOfElements > 1 ? createListR(numberOfElements - 1) : NULL;
    return elem;
}
void dumpList(node* iter)
{
    while(iter)
    {
        cout << setw(2) << iter->value << "|" << iter->value2 <<  endl;
        iter = iter->next;
    }
    cout << endl;
}
void wypiszListe(node* iter, node* koniec)
{
    if(iter==NULL) cout << "puste!" <<endl;
    if(iter==koniec) cout << "puste!" <<endl;
    while(iter!=koniec)
    {
        cout << setw(2) << iter->value << "|";
        iter = iter->next;
    }
    cout << endl;
}
node* gimmePivot(node* head)
{
    node* pivot = head;
    while(head!=NULL)
    {
        pivot = pivot->next;
        head = head->next;
        if(head!=NULL) head = head->next;
    }
    return pivot;
}
node* divideList(node* &head, node* koniec)
{
    node* pivot = head;
    node** mainIterator = &(head->next);
    node* firstHead = NULL;
    node** firstIterator = &firstHead;
    while((*mainIterator)!=koniec)
    {
        if((*mainIterator)->value <= pivot->value)
        {
            *firstIterator = *mainIterator;
            *mainIterator = (*mainIterator)->next;
            (*firstIterator)->next = NULL;
            firstIterator = &((*firstIterator)->next);
        }
        else mainIterator = &((*mainIterator)->next);
    }
    head = (firstHead == NULL) ? pivot : firstHead;
    *firstIterator = pivot;
    return pivot;
}
node* divideByPoping(node** head, node* ending)
{
    node* pivot = *head;
    node* fIter = pivot;
    node** przed = &fIter;
    node** sIter = &((*head)->next);
    while((*sIter)!= ending)
    {
        if((*sIter)->value < pivot->value)
        {
            node* tmp = *sIter;
            *sIter = (*sIter)->next;

            tmp->next = *przed;
            *przed = tmp;
            przed = &((*przed)->next);
        }
        else sIter = &((*sIter)->next);
    }
    *head = fIter;
    return pivot;
}
void quickSort(node** head, node* ending)
{
    if((*head)==ending || (*head)->next==ending) return;
    node* pivot = divideByPoping(&(*head), ending);
    quickSort(&(*head), pivot);
    quickSort(&(pivot->next), ending);
}
int main()
{
    srand (7);
    node* lista = createListR(10);
    dumpList(lista);
    node** lista1 = &lista;
    quickSort(lista1, NULL);
    cout << "sorted" << endl;
    dumpList(*lista1);
    return 0;
}
