#include <iostream>

using namespace std;
int how_many_ways(string map, int N) {
	int* res = new int[N];
	res[0] = (int)(map[0]-'0');
	for (int i = 1; i < N; i++) {
		res[i] = 0;
		if (map[i] == '1') {
			for (int j = 1; j <= 6 && j <= i; j++) {
				res[i] += res[i - j];
			}
		}
	}
	return res[N - 1];
}

int main() {
    int N;
    cin >> N;
    string map;
    cin >> map;
    if (map.size() != N) {
        cout << "Incorrect map size" << endl;
        return -1;
    }
    cout << how_many_ways(map, N) << endl;
}
